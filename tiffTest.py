#! /usr/bin/env python3

from PIL import Image, TiffImagePlugin
from sys import argv

script, filename = argv

im = Image.open( filename )

print( "\nOverall data" )
print( im.info )

tsts = {269: "\nDocument Name\t<Barcode>/<image filename>", 
        315: "\nPerson's name", 
        271: "\nScanner manufacturer", 
        272: "\nScanner model number", 
        305: "\nSoftware version", 306: "\nDate and time",}

for k, v in tsts.items():
    tval = im.tag[k] if k in im.tag else 'None'
    print(v, k, tval)
