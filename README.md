# Hathi Trust TIFF Metadata injection


## Contents

- [Purpose]
- [Downloading]
- [Pre-installation]
- [Installation]
- [Use]


## Purpose

This Python sketch is designed to take CSV metadata and inject it into a TIFF
header, according to Hathi Trust guidelines.


## Downloading

    cd ~/
    git clone https://bitbucket.org/mdill/tiff_metadata.git
    pip install -r req.txt


## Pre-installation

Once downloaded, it is important to modify the source for your region and
company.  There are two strings which need to be change:

1. CONST_CURRENT_AUTHOR
2. CONST_NON_DTS_OFFSET

These files are characteristic constants.  The `CONST_CURRENT_AUTHOR` string is
the name of your company, or the name of the person who will be scanning the
images.

The `CONST_NON_DTS_OFFSET` is guided by the same concept, but for when your
timezone is NOT taking part in DST.  As GMT doesn't change for DST, you will have
to add 1 to the `dstOffset` to get this correct.  An example of this for Eastern
Time (EST) wolud be `-5:00`, and would be input as "-5" for this constant.


## Installation

Once the appropriate changes have been made, you can implement the Makefile by
running `make` in your terminal, and you will be prompted for your `sudo`
password.  This is because you need access in order to write into the local `bin`
directory.  I urge you to look over the source, to verify that nothing sneaky is
going on behind the scenes.

This sketch also requires the installation of the
Python-Pillow, numpy, and pylibtiff librarys in
order to manipulate, and save, TIFF image file
data.  Finally, you need a version of Python 3.x
or higher, as versions of 2.7 or lower will not
work correctly.


## Use

If implemented correctly, the Makefile will make the executable program
available to your shell.  Full instructions are available throught the man pages
by `man HathiInject`.

The included spreadsheet can be used to create the CSV file, with correct column
data.

Once these steps have been created, you must create a folder of your TIFF files.
The CSV file will need to reflect this directory.  The files, in alpha-numeric
order will resemble the lines in the CSV file.

If this order is correct, you can run the program to inject the information
into each of the images, one-by-one.  The first image will receive the data from
the first line of the CSV, the second will receive the data from the second line,
and so on.

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/tiff_metadata/src/6675dc05036facb4807ddf017b6be061b01ec124/LICENSE.txt?at=master) file for
details.

[Purpose]:#markdown-header-purpose
[Downloading]:#markdown-header-downloading
[Pre-installation]:#markdown-header-pre-installation
[Installation]:#markdown-header-installation
[Use]:#markdown-header-use
