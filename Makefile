#
# `make`			Build executable file in the /usr/local/bin directory.
# `make mans`		Build the man page
# `make clean`		Removes the executable file
# `make cleanest'	Removes both the executable file as well as the man page
#
# *****************************************************************************

# Source files
#
SRCS = HathiInject.py
OBJDIR = /usr/local/bin
MAN = HathiInject.6

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
        MANDIR = /usr/local/man/man6
endif
ifeq ($(UNAME_S),Darwin)
        MANDIR = /usr/local/share/man/man6
endif

# *****************************************************************************

all: stuffs

# Copy the Python script, then make it executable
#
stuffs: mans
	sudo cp $(SRCS) $(OBJDIR)/HathiInject
	sudo chmod +x $(OBJDIR)/HathiInject
	sudo cp tiffTest.py $(OBJDIR)/tiffTest
	sudo chmod +x $(OBJDIR)/tiffTest

# Create a man page for the executable
#
mans:
	sudo mkdir -p $(MANDIR)
	sudo cp $(MAN) $(MANDIR)/$(MAN)
	sudo gzip $(MANDIR)/$(MAN)

# Remove the executable, just for insurance
#
clean:
	sudo rm $(OBJDIR)/HathiInject
	sudo rm $(OBJDIR)/tiffTest

# Remove the man page, as well as the executable
#
cleanest: clean
	sudo rm $(MANDIR)/$(MAN)

